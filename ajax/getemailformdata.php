<?php

include_once ('class/class.Log.php');
include_once ('class/class.ErrorLog.php');
include_once ('class/class.AccessLog.php');

//
// Build grid parameters passed in url
//
$page = $_GET['page']; // get the requested page
$limit = $_GET['rows']; // get how many rows we want to have into the grid
$sidx = $_GET['sidx']; // get index row - i.e. user click to sort
$sord = $_GET['sord']; // get the direction
if(!$sidx) 
	$sidx = 1;

$searchboolean = $_GET['_search'];
if ($searchboolean == 'true')
{
  $searchfield = $_GET['searchField'];
  $searchstring = $_GET['searchString'];
  $searchoper = $_GET['searchOper'];
  $filters = $_GET['filters'];
}
else
{
  $searchfield = "";
  $searchstring = "";
  $searchoper = "";
  $filters = "";
}

// Use this option to set common search rules. If not set all the available options will be used. All available option are: ['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn'] The corresponding texts are in language file and mean the following: ['equal','not equal', 'less', 'less or equal','greater','greater or equal', 'begins with','does not begin with','is in','is not in','ends with','does not end with','contains','does not contain','is null','is not null'] Note that the elements in sopt array can be mixed in any order.

//
// messaging
//
$returnArrayLog = new AccessLog("logs/");

$search = "";
if ($searchfield)
{
  switch ($searchoper) {
    case "bw":
        $search = " WHERE $searchfield LIKE '$searchstring%' ";
        break;

    case "bn":
        $search = " WHERE $searchfield NOT LIKE '$searchstring%' ";
        break;

    case "ew":
        $search = " WHERE $searchfield LIKE '%$searchstring' ";
        break;

    case "en":
        $search = " WHERE $searchfield NOT LIKE '%$searchstring' ";
        break;

    case "cn":
        $search = " WHERE $searchfield LIKE '%$searchstring%' ";
        break;
            
    case "nc":
        $search = " WHERE $searchfield NOT LIKE '%$searchstring%' ";
        break;

    case "in":
        $search = " WHERE $searchfield IN '$searchstring' ";
        break;
            
    case "ni":
        $search = " WHERE $searchfield NOT IN '$searchstring' ";
        break;
                    
    case "eq":
        $search = " WHERE $searchfield = '$searchstring' ";
        break;

    case "ne":
        $search = " WHERE $searchfield <> '$searchstring' ";
        break;    

    case "lt":
        $search = " WHERE $searchfield < '$searchstring' ";
        break;
        
    case "le":
        $search = " WHERE $searchfield <= '$searchstring' ";
        break;
            
    case "gt":
        $search = " WHERE $searchfield > '$searchstring' ";
        break; 

    case "gt":
        $search = " WHERE $searchfield >= '$searchstring' ";
        break; 

    case "nu":
        $search = " WHERE $searchfield IS NULL";
        break;
        
    case "nn":
        $search = " WHERE $searchfield IS NOT NULL";
        break;        
    
    default:
        $log = new ErrorLog("logs/");
        $log->writeLog("Search error: Search opperand not found. searchoper = $searchoper; searchfield = $searchfield, searchstring = $searchstring");
        $status = "Error";
        $rv = "";
        exit($rv);
    }
}

//------------------------------------------------------
// set db user info
//------------------------------------------------------

// open connection to host
$DBhost = "localhost";
$DBschema = "leah";
$DBuser = "leah";
$DBpassword = "leah";

//
// connect to db
//
$status = "";
$dbConn = @mysql_connect($DBhost, $DBuser, $DBpassword);
if (!$dbConn) 
{
  $log = new ErrorLog("logs/");
  $dberr = mysql_error();
  $log->writeLog("DB error: $dberr - Error mysql connect. Unable to get email list.");

  $status = "Error";
  $rv = "";
  exit($rv);
}

if (!mysql_select_db($DBschema, $dbConn)) 
{
  $log = new ErrorLog("logs/");
  $dberr = mysql_error();
  $log->writeLog("DB error: $dberr - Error selecting db Unable to get email list.");

  $status = "Error";
  $rv = "";
  exit($rv);
}

//
// get record count
//
$sql = "SELECT COUNT(*) AS count from emailcollectortbl";
if (!$sql_result = mysql_query($sql, $dbConn))
{
   $log = new ErrorLog();
   $sqlerr = mysql_error();
   $log->writeLog("SQL error: $sqlerr - Error doing select for count");
   $log->writeLog("SQL: $sql"); 
   $rv = "";
    exit($rv);
}

$row = mysql_fetch_array($sql_result,MYSQL_ASSOC);
$count = $row['count'];
if( $count > 0 ) 
{
	$total_pages = ceil($count/$limit);
} 
else 
{
	$total_pages = 0;
}

if ($page > $total_pages) 
	$page=$total_pages;

$start = $limit*$page - $limit; // do not put $limit*($page - 1)
$sql = "SELECT ID AS id, 
  name,
  email,
  organization,
  comment,
  interest1,
  interest2,
  interest3,
  DATE_FORMAT(enterdate,'%m-%d-%Y') as enterdate
  FROM emailcollectortbl 
  $search
  ORDER BY $sidx $sord 
  LIMIT $start, $limit";

if (!$sql_result = mysql_query($sql, $dbConn))
{
   $log = new ErrorLog();
   $sqlerr = mysql_error();
   $log->writeLog("SQL error: $sqlerr - Error doing select for data");
   $log->writeLog("SQL: $sql"); 
   $rv = "";
   exit($rv);
}

// debug
// print_r($page . "||");
// print_r($total_pages . "||");
// print_r($count . "||");
// print_r($sql . "||");
// print_r($sql_result . "||");
// die();
// debug

$responce =  new stdClass();
$responce->page = $page;
$responce->total = $total_pages;
$responce->records = $count;
$i=0;
while($row = mysql_fetch_array($sql_result,MYSQL_ASSOC)) 
{
    $responce->rows[$i]['id']=$row['id'];
         $responce->rows[$i]['cell']=array($row['name'],$row['email'],$row['organization'],$row['comment'],$row['interest1'],$row['interest2'],$row['interest3'],$row['enterdate']);
     $i++;
}        
echo json_encode($responce);  
//
//break;
?>