<?php

include_once ('class/class.Log.php');
include_once ('class/class.ErrorLog.php');
include_once ('class/class.AccessLog.php');

//
// Build grid parameters passed in url
//

// No info being passed in at tho time

//
// messaging
//
$returnArrayLog = new AccessLog("logs/");

//------------------------------------------------------
// set db user info
//------------------------------------------------------

// open connection to host
$DBhost = "localhost";
$DBschema = "leah";
$DBuser = "leah";
$DBpassword = "leah";

//
// connect to db
//
$status = "";
$dbConn = @mysql_connect($DBhost, $DBuser, $DBpassword);
if (!$dbConn) 
{
  $log = new ErrorLog("logs/");
  $dberr = mysql_error();
  $log->writeLog("DB error: $dberr - Error mysql connect. Unable to export email list.");

  $status = "Error";
  $rv = "";
  exit($rv);
}

if (!mysql_select_db($DBschema, $dbConn)) 
{
  $log = new ErrorLog("logs/");
  $dberr = mysql_error();
  $log->writeLog("DB error: $dberr - Error selecting db Unable to export email list.");

  $status = "Error";
  $rv = "";
  exit($rv);
}

$sql = "SELECT ID AS id, 
  name,
  email,
  organization,
  comment,
  interestlevel 
  FROM emailcollectortbl";

if (!$sql_result = mysql_query($sql, $dbConn))
{
   $log = new ErrorLog();
   $sqlerr = mysql_error();
   $log->writeLog("SQL error: $sqlerr - Error doing select for export data");
   $log->writeLog("SQL: $sql"); 
   $rv = "";
   exit($rv);
}

// debug
// print_r($sql . "||");
// print_r($sql_result . "||");
// die();
// debug

// delete any previous created csv files
array_map('unlink', glob("csv/*.csv"));

// open the filecsv
$filename = 'csv_' . date('Ymd') .'_' . date('His') . '.csv';
$fh = fopen('csv/' . $filename, 'w');

// Headings and rows
$headings = array('id','name','email','organization','comment','interestlevel');
fputcsv($fh, $headings);
        
// Loop over the items to export
while($row = mysql_fetch_array($sql_result,MYSQL_ASSOC)) 
{
    fputcsv($fh, $row);
}        

$fh = fclose($fh);        

// print $filename;
$fh = fopen('csv/' . $filename, 'r');
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment; filename='$filename';" );
header("Content-Transfer-Encoding: binary");  

fpassthru($fh);
?>