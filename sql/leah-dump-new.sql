-- MySQL dump 10.13  Distrib 5.5.35, for Linux (i686)
--
-- Host: localhost    Database: leah
-- ------------------------------------------------------
-- Server version	5.5.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `emailcollectortbl`
--

DROP TABLE IF EXISTS `emailcollectortbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailcollectortbl` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `organization` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `interest1` varchar(255) DEFAULT NULL,
  `interest2` varchar(255) DEFAULT NULL,
  `interest3` varchar(255) DEFAULT NULL,
  `enterdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=big5;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailcollectortbl`
--

LOCK TABLES `emailcollectortbl` WRITE;
/*!40000 ALTER TABLE `emailcollectortbl` DISABLE KEYS */;
INSERT INTO `emailcollectortbl` VALUES 
(1,'Tarrant Cutler','Sole proprietor ','Comment tarry','tc@gmail.com','learning','','subscribe','2014-08-25 13:16:51'),
(2,'Joe Fish','Bass Pro','Comment fish','jf@yahoo.com','learning','','subscribe','2014-08-25 13:16:51'),
(3,'Jane Manson','Maryjanes coffee','Comment Jane ','jm@ms.com','learning','','subscribe','2014-08-25 13:16:51'),
(4,'Steve Johnson','Fidelity ','Comment fidelity','sj@yahoo.com','learning','','subscribe','2014-08-25 13:16:51'),
(5,'Georgia Brown','Fire starters','Comments fire','gb@fire.com','learning','','subscribe','2014-08-25 13:16:51'),
(6,'John Doe','Survey organization. ','Comments jd','jd@gmail.com','learning','','subscribe','2014-08-25 13:16:51'),
(7,'Brenda Farve','Yahoo','','bf@yahoo.com','learning','','subscribe','2014-08-25 13:16:51'),
(8,'Joseph Stalin','Politburo ','Comment Russia ','js@ussr.com','learning','','subscribe','2014-08-25 13:16:51'),
(9,'Francis Marshal','French Embasy','Comments French ','fm@gmail.com','learning','','subscribe','2014-08-25 13:16:51'),
(10,'Louis Lane','Daily Planet','Comment louis','ll@paper.com','learning','','subscribe','2014-08-25 13:16:51'),
(11,'Jimmy Olson','Daily Planet','Comment jo','jo@gmail.com','learning','','subscribe','2014-08-25 13:16:51'),
(12,'Bailey Dog','Dog pound','Woof','bd@comcast.net','learning','','subscribe','2014-08-25 13:16:51'),
(13,'Jacob Marley','Accountatnt','Bah Humbug','jm@yahoo.com','learning','','subscribe','2014-08-25 13:16:51'),
(14,'Peter Graves','MI','Cant talk now','pg@gmail.com','learning','','subscribe','2014-08-25 13:16:51'),
(15,'John Adams','President','','ja@usa.gov','learning','','subscribe','2014-08-25 13:16:51'),
(16,'Jack Sprat','Fairy Tale','','js@yahoo.com','learning','','subscribe','2014-08-25 13:16:51'),
(17,'Abe Lincoln','USA','','al@usa.gov','learning','','subscribe','2014-08-25 13:16:51'),
(18,'Bill Clinton','USA','','bc@gmail.com','learning','','subscribe','2014-08-25 13:16:51'),
(19,'Semore Johnsom','Homer','','sj@ms.com','learning','','subscribe','2014-08-25 13:16:51'),
(20,'Go ga','Gigi ','Off ','gg@g.com','learning','','subscribe','2014-08-25 13:16:51'),
(21,'test','test','','test@test.com','learning','','subscribe','2014-08-25 13:16:51');
/*!40000 ALTER TABLE `emailcollectortbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emailinteresttbl`
--

-- DROP TABLE IF EXISTS `emailinteresttbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
-- CREATE TABLE `emailcollectortbl` (
--   `id` bigint(20) unsigned NOT NULL,
--   `interest` varchar(255) DEFAULT NULL
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=big5;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailinteresttbl`
--

-- LOCK TABLES `emailinteresttbl` WRITE;
/*!40000 ALTER TABLE `emailcollectortbl` DISABLE KEYS */;
-- INSERT INTO `emailcollectortbl` VALUES 
-- (1,'I’m interested in learning more about COVES on a one-time basis'),
-- (2,'I’m interested in possible becoming a member of COVES'),
-- (3,'I’m interested in subscribing to quarterly email updates');
/*!40000 ALTER TABLE `emailcollectortbl` ENABLE KEYS */;
-- UNLOCK TABLES;

--
-- Table structure for table `emailinteresttbl`
--

-- DROP TABLE IF EXISTS `emailcollectorinteresttbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
-- CREATE TABLE `emailcollectortbl` (
--   `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
--   `emailcollectorid` int(11) DEFAULT NULL,
--   `emailinterestid` int(11) DEFAULT NULL
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=big5;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailcollectortbl`
--

-- LOCK TABLES `emailcollectorinteresttbl` WRITE;
/*!40000 ALTER TABLE `emailcollectorinteresttbl` DISABLE KEYS */;
-- INSERT INTO `emailcollectortbl` VALUES 
-- (1,1),
-- (2,1),
-- (3,1),
-- (4,1),
-- (5,1),
-- (6,1),
-- (7,1),
-- (8,1),
-- (9,1),
-- (10,1),
-- (11,1),
-- (12,1),
-- (13,1),
-- (14,1),
-- (15,1),
-- (16,1),
-- (17,1),
-- (18,1),
-- (19,1),
-- (20,1),
-- (21,1);

/*!40000 ALTER TABLE `emailcollectortbl` ENABLE KEYS */;
-- UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-07 13:14:39
