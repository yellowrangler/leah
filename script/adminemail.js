$(document).ready(function() {
    // $('#gridTitle').click(function() {
    //     if ($(this).val() == "Show Title") 
    //     {
    //         $(this).val("Hide Title");
    //         $(".ui-jqgrid-titlebar").show();
    //     }
    //     else 
    //     {
    //         $(this).val("Show Title");
    //         $(".ui-jqgrid-titlebar").hide();
    //     }
    // });
    
    // $('#gridPagination').click(function() {
    //     if ($(this).val() == "Show Pagination") 
    //     {
    //         $(this).val("Hide Pagination");
    //         $(".ui-jqgrid-pager").show();
    //     }
    //     else 
    //     {
    //         $(this).val("Show Pagination");
    //         $(".ui-jqgrid-pager").hide();
    //     }
        
    // }); 

    $('#gridExport').click(function() {
        exportCSVfile();
    });
    
    buildUpperGrid();
        
    $("#emailGrid").jqGrid(
        'navGrid',
        '#emailPager',
        {
            search: true,
            edit:false,
            add:false,
            del:false
        }
    );

});

function exportCSVfile()
{
    var data = "";
    
   $.ajax({
            url:"ajax/exportemailformdata.php",
            data: data,
            type:"POST",
            success: function (rvar, status, xhr) {
                data = rvar;
                window.location="ajax/csv/"+data;
            },
            error: function (xhr, status, errorthrown) {
                alert("Error: Status"+status);
            }
        });
}


function buildUpperGrid()
{
    $("#emailGrid").jqGrid({
        url:"ajax/getemailformdata.php",
        datatype: 'json',
        colNames:['Name', 'email','Organization','Comment','Intest1','Intest2','Intest3','Date'],
        colModel : [
        {name:'name',index:'name', search:true, stype:'text', align:"left"},
        {name:'email',index:'email', align:"left"},
        {name:'organization',index:'organization', align:"left"},     
        {name:'comment',index:'comment', align:"left"},       
        {name:'interest1',index:'interest1', align:"center"},  
        {name:'interest2',index:'interest2', align:"center"},  
        {name:'interest3',index:'interest3', align:"center"},  
        {name:'enterdate',index:'enterdate', align:"center"}           
        ],
        rowNum:7,
        rowList:[10,20,30],
        pager: '#emailPager',
        sortname: 'name',
        scroll:true,
        autowidth: true,
        viewrecords: true,
        sortorder: "desc",
        caption:"Email Entries List"
    });
}
