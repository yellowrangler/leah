$(document).ready( function(){
    $("#email-form").submit(function (e) {
        // we need to do some validation and prep work first so we prevent default
        e.preventDefault();

        // get form data into string for ajax call
        var emailformstring = $("#email-form").serializeArray();
        // var emailformJSON = JSON.stringify(emailformstring);

        // alas safari does not support the required attribute 
        // so we must do redundant validation for required fields here
        var rc = validateEmailFormData(emailformstring);

        // save the data to db
        if (rc)
            saveEmailFormData(emailformstring);

    });
});  

function validateEmailFormData(emailformstring) {

    var rv = true;
    var msg = "";
    var msgTitle = "Required Field";
    var msgLead = "The following items are required:<br /><ul>";
    var interesthasbeenselected = "";

    // 
    // required fields are validated
    //
    var arr = emailformstring;
    $.each(arr, function(i, el) {
        switch (el.name)
        {
            case "email-name":
                if (el.value == "")
                {
                    if (msg == "")
                    {
                        msg = msgLead;
                    }

                    msg = msg + "<li>Name</li>";
                }
                break;

            case "email-address":
                if (el.value == "")
                {
                    if (msg == "")
                    {
                        msg = msgLead;
                    }

                    msg = msg + "<li>Email address</li>";
                }
                break; 

            case "email-organization":
                if (el.value == "")
                {
                    if (msg == "")
                    {
                        msg = msgLead;
                    }

                    msg = msg + "<li>Organization</li>";
                }
                break;  

            case "email-interest1":
                if (el.value != "" && el.value != 0)
                {
                    interesthasbeenselected = "1";
                }
                break; 

            case "email-interest2":
                if (el.value != "" && el.value != 0)
                {
                    interesthasbeenselected = "1";
                }
                break; 
                
             case "email-interest3":
                if (el.value != "" && el.value != 0)
                {
                    interesthasbeenselected = "1";
                }
                break;                 
        }
    }); 

    // 
    // at least one level of interest check box must be seleted
    //
    if (interesthasbeenselected == "")
    {
        if (msg == "")
        {
            msg = msgLead;
        }
        msg = msg + "<li>You must select a Level of interest</li>";
    }
    
    //
    // if we had any errors send out dialog
    //
    if (msg !== "")
    {
        msg = msg + "</ul>";
        showDialogErr(msgTitle, msg);
        rv = false;
    }


    return rv;
}

function saveEmailFormData(data) {
    var msg = "";
    var msgTitle = "";

	$.ajax({
            url:"http://www.yellowrangler.com/mos/leah/ajax/saveemailformdata.php",
            data: data,
            type:"GET",
            dataType:"jsonp",
            contentType: "application/json",
            success: function (rvar, status, xhr) {
                var rv = rvar;
                
                if (rv == "Ok") 
                {
                    $('#email-form').trigger("reset");

                    msg = "<center>You have successfully added your information to COVES.<br /><br />We’ve received your contact information for COVES and will be in touch with you soon.<br /><br />Thank you.</center>";
                    msgTitle = "Success";

                    showDialogSuccess(msgTitle,msg);
                    // alert("Success: We were able to add your information.");
                }
                else 
                {
                    msg = "We were unable to add your information to Coves! <br /><br />Please try again at a later time.";
                    msgTitle = "Processing Error";

                    showDialogErr(msgTitle,msg);
                    // alert("Error: Unable to add your information - Please try again later.");
                }
            },
            error: function (xhr, status, errorthrown) {
                msg = "We were unable to add your information to Coves! <br /><br />Please try again at a later time.";
                msg = msg + "<br /><br /> Error status: " + status;
                msgTitle = "System Error";

                showDialogErr(msgTitle,msg);
                // alert("Error: Status"+status);
            }
        });
}

function showDialogErr(msgTitle,msg) {
    $('#email-modal-error-dialog').modal('show');
    $("#modal-error-dialog-title").html(msgTitle);
    $("#modal-error-dialog-msg").html(msg);
}

function showDialogSuccess(msgTitle,msg) {
    $('#email-modal-success-dialog').modal('show');
    $("#modal-success-dialog-title").html(msgTitle);
    $("#modal-success-dialog-msg").html(msg);
}